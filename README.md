# t2lm
fortgeschrittenen Praktikum Versuch T2 - Mathilde und Leo

weil alex ist uncool

# ToDo


+ Abweichung vom Literaturwert ?

- Unsciherheit auf Delta E ist ziemlich klein !

- Effizenz unsicherhiet zu klein, intervalle Größer machen?

- Unsicherheiten auf den Channel => Unsicherheit auf den Faktor
- Unsicherheiten auf Winkel
- Offsets bei konventionell und zyklisch überprüfen


4.2.3 falsch gemacht?! neue Formel und Rechnung 
    + nu benutzten

4.2.4 Streuwirkungsquerschnitt muss noch bestimmt werden 
    + für edelstahl 
    + vergleich mit aluminium

4.2.5 Elektronenmasse daraus bestimmen 
    + kontrollieren
    + und richtige Rechnung?

- systematische Unsicherheiten

- E_gamma vorher literatur wert oder messwert?

# Auswertung:

+ Direkt:
- Diskussion des Offsets (im Fit und wir dürfen es abziehen aber argumentieren warum)
- Diskussion Form an Nah Cs137 (Direkt)
- Diskussion des Residuum von Cs137 (Welle sichtbar)
- Kalibrationskurve: residuum von Europium 4, mögliche verschiebung nach links durch anteil von europium 5 peak ? wird trotzdem behalten

- Diskussion Effizenz: so niedrig, da sich da wir nur die Peakereignisrate benutzen und nicht die links und rechts davon (Kein integral trotz verteilung)


- Diskussion verwenden von der Messschiene statt Messband 



# Sonstiges:

- 135 metallstift nicht aus dem aufbau entfernt ?!?!?!




# Leos Lustige Latex Lektüre
* Peaksbestimmen:
    + Messwerte sind die Rohdaten/liveTime - Hintergrund/liveTimeHintergrund (also Ereignisrate statt Ereignisse)
        - Unsicherheiten auf die Messdaten ist die sqrt(Binhöhe) (bevor dem normieren auf die Zeit, aber wird fortgepflanzt)
    + benutzen 2 messreihen nah und fern
        * erklären warum wir 2 haben
    + an diese Messdaten passen wir dann an den ausgesuchten Intervallen Gaußkurven an:
        - hierbei passen wir die Fkt B[2] * g(B[0]=µ,B[1]=std) + B[3] = Fit an
        - B[0] ist der Mittelwert (kanal beim Peak) (Guess durch max wert)
        - B[1] ist die Standardabweichung  (Guess 1/4 der Intervallbreite, da wir annehmen, dass wir ca 2 std zur jeder Seite gehen)
        - B[2] multiplikator, da nicht auf 1 normiert (guess ist die summation über die bins im intervall)
        - B[3] ist offset, physikalische Argumentation: durch benachbarte Peaks unter anderen Radioaktiven Hintergund (diese Argumentation funktioniert bei direkt sehr gut, bei konventionell müssen wir das untersuchen)
        - wir bekommen ebenfalls die unsicherheiten durch den fit

        - zudem bestimmen wir mathematisch die Halbwertsbreite der Anpassung: ∆C =  2*np.sqrt(2*np.log(2))* std
            * wir bestimmen alle verwendeten Größen anhand der Anpassung
        + beispielhafte anpassung zeigen und Cäsium 173 mit der welle erklären?????
        + und chiq und so erläutern

    + diese peaks benutzen wir für die kalibrationskurve C = Faktor*E + Offset
        - C auf y, da fehlerbehaftet
        - knick um kanal 800 => cutoff nach 820
            * residuum plot und die längere argumentation vom Wochenende
        - eu152 Peak 4 ist zu nah an peak 5 und wird daher verschoben, daher nehmen wir ebenfalls diesen aus der Asuwertung
        - na22 peak 1 wird ausgelassen, da wir mit diesen nachher die masse des elektrons bestimmen 
        - verschiednen auswertungen (deren steigung und chiq zeigen)

        + zur Umrechung von Channel zur Energie, daher wird umgefort und fehlerfortpflanzung
        + wir werden im weiteren nur die anpassung unterhlab des cutoff channels verwenden, da dort die anpassung gut ist und in diesem bereich unsere wichtigen werte liegen

    # ab hier nur noch werte die unter cutoff liegen

    + Energie auflösung 
        # brauchen wir hier noch eine darstellung von ∆E gegen E oder so oder reicht der plot in der gaußanpassung
        - zur bestimmung der Energie formen wir die gleichung zu ∆E^2/E = a^2*E+b^2 um 
        - ∆E bekommen wir aus ∆C, genau so wie E
        - dann mit Fehlerfortpflanzung lineare Regression und bestimmung der Faktoren und deren Unsicherheiten
        - ergebnisse diskurtien 
        # evtl ergebnisse erneut überarbeiten??
    
    + Effizenz
        - a(t) bekommen wir aus dem alter der proben, alter nenne? und aktivität nennen  (+Unsicherheit)
        - energie aus Kanal nummer
        - als m benutzen wir den faktor B[2], da dieser die Ereignisrate pro sekunde gibt (praktisch integral über die Gaußfunktion, daher bekommen wir die anzahl an ereignissen, die zu diesem peak gehören)
        # effizent Unsicherheiten überarbeiten und schauen warum der letzt abhebt zudem anpassung überarbeiten
        - linearer Zusammenhang sollte gegeben sein
    
    + Masse Elektronen aus Na22 peak1
        + entsteht durch beta+ strahlung und annhilation
        + in natürlichen einheiten 
        + 2 verschiedene werte für nah und fern, hier evtl diskutieren woher eine abweichung kommen könnte, aber vergleich mit der literatur sieht gut aus



 



