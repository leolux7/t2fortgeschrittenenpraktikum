### Settings ###

true = True
false = False

class Settings:
    def __init__(self):

        self.offset = True

        ### Daten
        self.eigeneDaten = True

        ##### Analysen #####
        self.direkt = True
        self.konventionell = True
        self.zyklisch = True

        ##### Plots uns Ausgaben #####
        self.plots_allowed = True
        self.saveOrShow = False #save = True

        ## Allgemein Analyse ##
        self.printEinlesen = False
        self.printAnpassungGaus = False
        self.printAnpassungLin = False

        pa = self.plots_allowed
        

        ## Direkt ##
        self.direktChannelTrennung = True and self.eigeneDaten
        self.plotGaussDirekt = pa and False

        self.withEu4Peak = False # wenn False, dann wir der Eu4 Peak in der small anpassung nicht verwendet

        # (1.2) Kalibrierungskurve
        self.latexTabellePeakChannel = False
        self.plotsEnergieChannelAnpassung = pa and False
        self.plotsEnergieLiteratur = pa and False
        self.printLinCE = False
        self.latexTablleKalibration = False #Werte der Anpassung mit unsicherheiten und Güte (alle werte (klein groß mittel))

        
        
        # (1.3) ∆E für Energieauflösung [check again!!]
        self.latexTabelleDeltaEs = False #Tabelle mit den ∆Es 
        self.printDeltasInput = False
        self.plotDeltaChannel = pa and False
        self.printDeltasOutput = False
        self.plotLinDeltaDirket = pa and False
        self.latexTabelleEnergieaufloesung = False

        # (1.4) Effizenz [Fehlerrechnung]
        self.latexTabellePeakHeight = False
        self.printOutputEffizenz = False
        self.plotOutputEffizenz = pa and False
        self.latexTabelleInensity = False


        # (1.5) Elektronen Masse
        # latex ergebnis
        self.printMasseElektron = False
        self.latexMasseElektron = False





        ## Konventionell ##
        self.plotGaussKonventionell = pa and False
        self.plotKonventionellGesamt = pa and False

        ## Zyklisch  ##
        self.plotGaussZyklisch = pa and False
        self.plotZyklischGesamt = pa and False

        #Auswertung Compton
        self.auswertungCompton = True

        # (2.2) Zusammenhang Peak-Energie und Winkel
        self.plotWinkelPeakEnergy = pa and False 

        #(2.3) Wirkungsquerschnitt
        self.plotWirkungsquerschnitt = pa and True

        #(2.4) m_e aus Compton
        self.plotM_eCompton = pa and True
