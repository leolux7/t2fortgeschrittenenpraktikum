# -*- coding: utf-8 -*-

#t=[]
# for j in range(len(f[0])): t.append(f[0][j][0][0])
# [width=0.95\textwidth]
import numpy as np




#Unsicherheiten
#Wenn daten keine listen sind
#----------------------------------------------------------------------------------------------------
def latextabelle(*data,runden=None,headline=None,complete=True,einheiten=None,caption="",label="",autoMessreihen=False,DataLengthUngleich=False):
    """
    Alles was mit eine \ dargestellt werden soll muss mit \\ eingegeben werden
    
    data: Daten die ausgegeben sollen (können mehrer Listen sein)
    runden: entsprechend dem .format formalismus (als Liste)
    headline: die Teile der Überschrift
    complete: Komplette Latex Tabelle oder nur die tatsächliche Liste (True oder False; 1 oder 0) 
    einheiten: Liste der Einheiten die hinter den Daten dargestellt werden sollen
    caption & label selbst erklärend
    autoMessreihen: baut header und und erste Spalte automatisch
    DataLengthUngleich: wenn Datenreihen unterschiedliche längen haben 
    """

    if autoMessreihen==True:
        if type(einheiten) != list:
            einheiten = [einheiten]
        if len(data)==1:
            data = list(data[0])
        else:
            data = list(data)
            
        
        headline = ["Messung"]
        for i in range(len(data)):
            headline.append("{0}. Messreihe".format(i+1))
        
        if len(einheiten) != len(data):
            einheiten = einheiten*(len(data))
        
        if runden != None:
            if type(runden)!= list:
                runden = [runden]
            
            if len(runden)==1:
                runden = runden * len(data)
            runden.insert(0,"")

        einheiten.insert(0,"")

        data.insert(0,[x+1 for x in range(len(data[0]))])
    

    #Fehler prüfung
    for i in range(len(data)-1):
        if DataLengthUngleich==False:
            if len(data[i])!=len(data[i+1]):
                print("Fehler: Daten haben nicht die selbe Länge!")
                print(str(i+1)+" und "+str(i+2))
                return None     
    if DataLengthUngleich==True: 
        data_length = 0
        for i in range(len(data)):
            if data_length < len(data[i]):
                data_length = len(data[i])
            
        for i in range(len(data)):
            data[i] = list(data[i])
            while len(data[i]) < data_length:
                data[i].append("")
    
    if headline!=None:
        if len(headline)!= len(data):
            print("Fehler: Überschrift {0} und Daten {1} haben nicht die selbe Länge!".format(len(headline),len(data)))
            return None
    if einheiten!=None:
        if len(einheiten)!= len(data):
            print("Fehler: Einheit und Daten haben nicht die selbe Länge!")
            return None
    
    if runden!=None:
        if type(runden) != list:
            runden = [runden]
        
        if len(runden)!= len(data):
            print("Fehler: Runden und Daten haben nicht die selbe Länge!")
            return None
    
    if runden==None:
        runden = [""]*len(data)
    
    
    if einheiten==None:
        einheiten=[""]*len(data)
    


    
    #--------------------------
    
    start = "\t \t "
    end = " \\\\"
    
    #Latex Grundlagen Start
    if complete==True:
        print("\\begin{table}")
        print("\t\\centering")
        print("\t\\begin{tabular}{c"+"|c"*(len(data)-1)+"}")
    
    #Überschrift
    if headline!=None:
        headline_string = headline[0]
        del headline[0]
        for i in range(len(headline)):
            headline_string = headline_string + " & "+ str(headline[i])
        headline_string = start + headline_string + end    
        print(headline_string)
    
    print(start+"\\hline")
    
    
    #Daten in Zeilen eintragen
    for i in range(len(list(data[0]))):
        zeilen_string = ""
        
        if einheiten[0] != "" and data[0][i]!="":
            zeilen_string = "$\\SI{"+"{0:{1}}".format(data[0][i],str(runden[0]))+"}{"+einheiten[0]+"}$"
        elif type(data[0][i])!=str:
            zeilen_string = "$"+"{0:{1}}$".format(data[0][i],str(runden[0]))
        else:
            zeilen_string = ""+"{0:{1}}".format(data[0][i],str(runden[0]))
        
        for j in range(1,len(data)):
            if einheiten[j] != "" and data[j][i] != "":
                zeilen_string = zeilen_string + " & " + "$\\SI{"+"{0:{1}}".format(data[j][i],str(runden[j]))+"}{"+str(einheiten[j])+"}$"
            elif type(data[j][i])!=str:
                zeilen_string = zeilen_string + " & " +"${0:{1}}$".format(data[j][i],str(runden[j]))
            else: 
                zeilen_string = zeilen_string + " & " +"{0:{1}}".format(data[j][i],str(runden[j]))
        zeilen_string = start + zeilen_string + end 
        print(zeilen_string)     
    
        
    if complete==True:
        #Latex Grundalgen Ende
        print("\t\\end{tabular}")    
        print("\t\caption{"+str(caption)+"}")
        print("\t\label{tab:"+ str(label)+"}")
        print("\end{table}")
    pass
 

#----------------------------------------------------------------------------------------------------

test  = [0.000001,2.000001,3.00002,4,5,6,7]
test2 = [1,2,3,4,5,6,7]
test3 = list(range(8))



#Beispiel
#latextabelle(test,test2,headline=["Spalte 1","Spalte 2",])#,einheiten=["mV","\\Omega"],complete=True,caption="Beispiel Caption",label="Beispiel")
#latextabelle(test,test2,headline=["Spalte 1","Spalte 2",],einheiten=["mV","\\Omega"],complete=False,caption="Beispiel Caption",label="Beispiel")
#latextabelle(test,headline=["Spalte 1"],einheiten=["mV"],complete=True,caption="Beispiel Caption",label="Beispiel")
#latextabelle(test,test2,runden=["2.2f","2.2f"],headline=["Spalte 1","Spalte 2",],complete=True,caption="Beispiel Caption",label="Beispiel")
#latextabelle(test,test2,headline=["Spalte 1","Spalte 2",],complete=True,caption="Beispiel Caption",label="Beispiel")

# if Set.latexTablleKalibration:
#         print("+"*50+"\n" + "LATEX TABELLE: Ergebnisse Kalibration" + "\n"+ "+"*50+ "\n")
#         headline = []
#         runden = []
#         caption = []
#         labelLatex = []
#         latextabelle(
#                     source[x],energyDataLiteratur[x],channelData[x],channelDataError[x], channelWidth[x], channelWidthError[x], chiq_ndof[x],
#                     headline=headline,
#                     runden=runden,
#                     caption=caption,
#                     label=labelLatex
#                     )

#         print("\n"+"+"*50)
#         print("Achtung! keine Trennung zwischen Nah und Fern")
#         print("+"*50+"\n" +22*"+" + " ENDE "+22*"+" + "\n"+ "+"*50+2*"\n")