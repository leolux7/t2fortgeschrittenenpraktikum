#Aktuelle Version von 5.03.22
import matplotlib.pyplot as plt 
import numpy as np
from analyse import wirkungsquerschnitt

from dataClass import Messung, Probe, Messreihe
from analyse import gaussAnpassung, linAnpassung, effizienz, quadAnpassung, energyWinkelKurve, gf, kleinNishaFormel
from settings import Settings
from scipy.constants import c
from fehlerfortpflanzung import err_kleinNishina, err_wirkung, absorption_err, N_e_err

#latextabellen Tool 
from latexTabellen import latextabelle

base = ".."

print("*"*30+" START " + "*"*30)

### Proben 
#für direkt
Co60 = Probe("Co60")
Cs137w = Probe("Cs137w")
Eu152 = Probe("Eu152")
Na22 = Probe("Na22")
probenDirekt = [Co60, Cs137w, Eu152, Na22]
#für ring und konventionell
Cs137s = Probe("Cs137s")

Set = Settings()


if Set.eigeneDaten:
    ### Eigene Daten
    #direkt
    dateinamenNah = [base + "/data/direkt/Nah_{0}.Tka".format(x.name) for x in probenDirekt] 
    dateinamenBackgroundNah = [base + "/data/direkt/Nah_Rausch.Tka"] 
    dateinamenFern = [base + "/data/direkt/Fern_{0}.Tka".format(x.name) for x in probenDirekt]
    dateinamenBackgroundFern = [base + "/data/direkt/Fern_Rausch.Tka"] 
    dateinamenDirekt = dateinamenNah + dateinamenFern
    dateinamenBackgroundDirekt = dateinamenBackgroundNah + dateinamenBackgroundFern
    messtypDirekt = ["direkt_Nah", "direkt_Fern"]
    #Anpassbare Parameter
    grenzChannel = 820 #820 
    # hier beschränken wir uns auf eine der Anpassungen (0: normal, 1: small, 2: big)
    wahlAnpassung = 1

    intervallPeakArrayDirekt = [
                        [[820,880],[905,975]],  #Co60 Pekas
                        [[430,560]],            #Cs137w Peaks
                        [[93, 125], [245, 305], [551, 615], [682-10, 744+5], [770, 860],[935, 1020]], #Eu152 Peaks
                        [[350, 423],[860,950]]] #Na22 Peaks
    
    #konventionell
    winkelKonventionell = ["50","65","80","105","135"]
    dateinamenKonventionellAl = [base + "/data/konventionell/{0}grad_Alu.TKA".format(x) for x in winkelKonventionell]
    dateinamenKonventionellFe = [base + "/data/konventionell/{0}grad_Eisen.TKA".format(x) for x in winkelKonventionell]
    dateinamenBackgroundKonventionell = [base + "/data/konventionell/{0}gradHintergrund.TKA".format(x) for x in winkelKonventionell]

    intervallPeakArrayFe = [[250, 400], [200, 350], [200, 300], [175, 250], [125, 190]]
    intervallPeakArrayAl = [[250, 400], [200, 350], [200, 300], [175, 250], [125, 190]]
    
    #zyklisch
    winkelZyklisch = ["10","20","30","40","50"]
    dateinamenZirkular = [base + "/data/zyklisch/{0}grad.TKA".format(x) for x in winkelZyklisch]
    dateinamenBackgroundZirkular = [base + "/data/zyklisch/{0}gradHintergrund.TKA".format(x) for x in winkelZyklisch]

    intervallPeakArray = [[435, 545], [405, 515], [370, 480], [330, 435],[290,400]]

else: 
    ### Alex Daten
    #direkt
    dateinamenNah = [base + "/dataAlex/direkt/Nah_{0}.Tka".format(x.name) for x in probenDirekt]
    dateinamenBackgroundNah = ["../dataAlex/direkt/Nah_Rausch.Tka"]
    dateinamenDirekt = dateinamenNah 
    dateinamenBackgroundDirekt = dateinamenBackgroundNah 
    messtypDirekt = ["direkt_Nah"]
    #Anpassbare Parameter
    grenzChannel = 1200
    wahlAnpassung = 0

    intervallPeakArrayDirekt = [
                        [[780, 860],[880, 980]], 
                        [[435,505]],    
                        [[90,110],[230, 275],[519, 580],[625,713],[725, 815],[930,1020]],  
                        [[340, 392],[827, 937]]]

    #konventionell
    winkelKonventionell = ["50","65","80","105","135"]
    dateinamenKonventionellAl = [base + "/dataAlex/konventionell/{0}Grad_Alu.TKA".format(x) for x in winkelKonventionell]
    dateinamenKonventionellFe = [base + "/dataAlex/konventionell/{0}Grad_Eisen.TKA".format(x) for x in winkelKonventionell]
    dateinamenBackgroundKonventionell = [base + "/dataAlex/konventionell/{0}Grad_Rausch.TKA".format(x) for x in winkelKonventionell]

    peaks = np.array([322, 325, 310, 320, 250, 230, 213, 190, 150, 160] )
    intervallPeakArrayAl = []
    intervallPeakArrayFe = []
    for i, peak in enumerate(peaks): 
        if i%2==0:
            intervallPeakArrayAl.append([peak-50,peak+50])
        else: 
            intervallPeakArrayFe.append([peak-50,peak+50])
    

    #intervallPeakArrayFe = [[250, 400], [200, 350], [200, 300], [175, 250], [125, 190]]
    #intervallPeakArrayAl = [[250, 400], [200, 350], [200, 300], [175, 250], [125, 190]]

    #ring
    ring = ["mittelrRing"]*4
    ring.append("großerRing")
    winkelZyklisch = ["10","20","30","40","50"]
    dateinamenZirkular = [base + "/dataAlex/zyklisch/{0}_{1}Grad.TKA".format(ring[i],x) for i,x in enumerate(winkelZyklisch)]
    dateinamenBackgroundZirkular = [base + "/dataAlex/zyklisch/{0}_{1}Grad_Rausch.TKA".format(ring[i],x) for i,x in enumerate(winkelZyklisch)]
    
    intervallPeakArray = [[435, 545], [405, 515], [370, 480], [330, 435],[290,400]]

    ### Alex Daten Ende

if Set.direkt:

    peakEnergy = [Co60.peakEnergy, Cs137w.peakEnergy, Eu152.peakEnergy, Na22.peakEnergy]

    def nahOrFernIndex(index):
        if index < len(dateinamenNah):
            return 0
        else:  
            return 1

    messungenDirekt = []
    figEnergyChannel, axs = plt.subplots(2,1,figsize=(20,10))
    for i in range(len(dateinamenDirekt)):
        if Set.printEinlesen or Set.printAnpassungGaus:
            print("\nMessung Direkt:",i+1,"[",probenDirekt[i%len(dateinamenNah)].name,"]" )
            print("Datei:   " + dateinamenDirekt[i])
        # einlesen und definieren der Messreihen mit Korrektur Offset
        messungenDirekt.append(Messung(dateinamenDirekt[i], dateinamenBackgroundDirekt[nahOrFernIndex(i)],messtypDirekt[nahOrFernIndex(i)],probenDirekt[i%len(dateinamenNah)].name ,probenDirekt[i%len(dateinamenNah)]))
        
        messungenDirekt[i].anpassungen = []
        messungenDirekt[i].deltaC = []
        
        for j, intervall in enumerate(intervallPeakArrayDirekt[i%len(dateinamenNah)]):
            if Set.printEinlesen or Set.printAnpassungGaus: print("Peak: ", j+1)
            anpassung = gaussAnpassung(messungenDirekt[i],intervall, Set.plotGaussDirekt, "Peak"+str(j+1), halbwert=True)
            messungenDirekt[i].anpassungen.append(anpassung[0])
            messungenDirekt[i].deltaC.append(anpassung[1])


        plt.close("all")
        
        # plotten aller Daten
        #messungenDirekt[i].plotData("Nah & Direkt: "+ probenDirekt[i].name,"nah_dirket_"+ probenDirekt[i].name)

    ### Bestimmung zusammenhang Channel und Energie
    #Energy = x * Channel + o
    
    if Set.direktChannelTrennung:
        nameAnpassung = ["normal", "small", "big"]
    else: 
        nameAnpassung = ["normal"]
    source = [ [] for _ in range(len(nameAnpassung)) ]
    energyDataLiteratur = [ [] for _ in range(len(nameAnpassung)) ]

    channelData = [ [] for _ in range(len(nameAnpassung)) ]
    channelWidth = [ [] for _ in range(len(nameAnpassung)) ] # Halbwertsbreite des Channels
    channelWidthError = [ [] for _ in range(len(nameAnpassung)) ]
    channelDataError = [ [] for _ in range(len(nameAnpassung)) ]

    chiq_ndof =  [ [] for _ in range(len(nameAnpassung)) ]
    
    faktor = [ [] for _ in range(len(nameAnpassung)) ]
    faktorError = [ [] for _ in range(len(nameAnpassung)) ]
    offset = [ [] for _ in range(len(nameAnpassung)) ]
    offsetError = [ [] for _ in range(len(nameAnpassung)) ]
    
    chiq_ndof_kalibration = [ [] for _ in range(len(nameAnpassung)) ]
    indexNa1 = [ [] for _ in range(len(nameAnpassung)) ] #index von Na1 in den listen
    indexEu4 = [ [] for _ in range(len(nameAnpassung)) ] #index von Na1 in den listen

    for i in range(len(messungenDirekt)):
        for j in range(len(messungenDirekt[i].anpassungen)):
            
            channelData[0].append(messungenDirekt[i].anpassungen[j].output.beta[0])
            channelDataError[0].append(np.sqrt(messungenDirekt[i].anpassungen[j].output.cov_beta[0][0]))
            channelWidth[0].append(messungenDirekt[i].deltaC[j][0])
            channelWidthError[0].append(messungenDirekt[i].deltaC[j][1])

            chiq_ndof[0].append(messungenDirekt[i].anpassungen[j].output.res_var)

            source[0].append(messungenDirekt[i].Probe.name +" Peak: " +str(j+1))
            energyDataLiteratur[0].append(messungenDirekt[i].Probe.peakEnergy[j])


            if Set.direktChannelTrennung:
                if messungenDirekt[i].anpassungen[j].output.beta[0] < grenzChannel:
                    channelData[1].append(messungenDirekt[i].anpassungen[j].output.beta[0])
                    channelDataError[1].append(np.sqrt(messungenDirekt[i].anpassungen[j].output.cov_beta[0][0]))
                    channelWidth[1].append(messungenDirekt[i].deltaC[j][0])
                    channelWidthError[1].append(messungenDirekt[i].deltaC[j][1])  
                    source[1].append(messungenDirekt[i].Probe.name +" Peak: " +str(j+1))
                    energyDataLiteratur[1].append(messungenDirekt[i].Probe.peakEnergy[j])
                    chiq_ndof[1].append(messungenDirekt[i].anpassungen[j].output.res_var)
                elif messungenDirekt[i].anpassungen[j].output.beta[0] > grenzChannel:
                    channelData[2].append(messungenDirekt[i].anpassungen[j].output.beta[0])
                    channelDataError[2].append(np.sqrt(messungenDirekt[i].anpassungen[j].output.cov_beta[0][0]))
                    channelWidth[2].append(messungenDirekt[i].deltaC[j][0])
                    channelWidthError[2].append(messungenDirekt[i].deltaC[j][1])
                    source[2].append(messungenDirekt[i].Probe.name +" Peak: " +str(j+1))
                    energyDataLiteratur[2].append(messungenDirekt[i].Probe.peakEnergy[j])
                    chiq_ndof[2].append(messungenDirekt[i].anpassungen[j].output.res_var)
    
   
    if Set.latexTabellePeakChannel:
        print("+"*50+"\n" + "LATEX TABELLE: Auswertung Peaks direkt" + "\n"+ "+"*50+ "\n")
        x = 0 # wahlAnpassung
        # x = 0 alle Messpunkte ausgeben
        # chi^2 / ndof und DeltaC angeben?

        headline=["Probe und Peak", "$E_{\\gamma, Liz.}$ in $\\si{keV}$", "Kanalnummer des Peaks", "Unsicherheit auf den Kanal", "$\\Delta C$", "$\\sigma_{\\Delta C}$", "$\\Chi^2/n_{dof}$" ]
        runden = ["","",".3f",".3f" ,".3f", ".3f",".2f"]
        caption = "In dieser Tabelle sind die durch unsere Gaussanpassung bestimmten Kanalnummern der Peaks und dereen Unsicherheiten angegeben. Der erste Block ist von der nahen Messung und der zweite Block ist von der fernen Messung. Zudem wird die Halbwertskanalbreite der Peaks und die Unsicherheit auf diese angeben, sowie die Güter der Anpassung."
        labelLatex = "auswertungPeaksDirekt"
        latextabelle(
                    source[x],energyDataLiteratur[x],channelData[x],channelDataError[x], channelWidth[x], channelWidthError[x], chiq_ndof[x],
                    headline=headline,
                    runden=runden,
                    caption=caption,
                    label=labelLatex
                    )

        print("\n"+"+"*50)
        print("Achtung! keine Trennung zwischen Nah und Fern und Runden")
        print("+"*50+"\n" +22*"+" + " ENDE "+22*"+" + "\n"+ "+"*50+2*"\n")



    for k in range(len(nameAnpassung)):
        if "Na22 Peak: 1" in source[k]:
            indexNa1[k] = [i for i,s in enumerate(source[k]) if s == "Na22 Peak: 1"]
        if "Eu152 Peak: 4" in source[k] and not Set.withEu4Peak and k == 1:
            indexEu4[k] = [i for i,s in enumerate(source[k]) if s == "Eu152 Peak: 4"]

        energyDataLiteraturONa1 = [x for i,x in enumerate(energyDataLiteratur[k]) if i not in (indexNa1[k]+indexEu4[k])]
        channelDataONa1 = [x for i,x in enumerate(channelData[k]) if i not in (indexNa1[k]+indexEu4[k])]
        channelDataErrorONa1 = [x for i,x in enumerate(channelDataError[k]) if i not in (indexNa1[k]+indexEu4[k])]
        sourceONa1 = [x for i,x in enumerate(source[k]) if i not in  (indexNa1[k]+indexEu4[k])]

        output = linAnpassung( energyDataLiteraturONa1, channelDataONa1, eyValues=channelDataErrorONa1, plot=Set.plotsEnergieChannelAnpassung,name="Normal").output
        faktor[k], offset[k] = output.beta
        faktorError[k], offsetError[k] = [np.sqrt(output.cov_beta[i][i]) for i in [0,1]]
        chiq_ndof_kalibration[k] = output.res_var

        if Set.printLinCE:
            if k == wahlAnpassung:
                print("Diese Anpassung wird weiter verwendet!")
            print(nameAnpassung[k], ) 
            print("     Faktor:    {0:.4f} ± {1}".format(faktor[k],faktorError[k]))
            print("     Offset:    {0:.4f} ± {1:.4f}".format(offset[k],offsetError[k]))
            print("\n"*2)

    if Set.latexTablleKalibration:
        print("+"*50+"\n" + "LATEX TABELLE: Ergebnisse Kalibration" + "\n"+ "+"*50+ "\n")
        anpassungenNamenLatex = ["alle","unter Kanal {0}".format(grenzChannel), "über Kanal {0}".format(grenzChannel)]
        headline = ["ausgewählte Peaks", "$a$", "$\\sigma_{a}$","$b$","$\\sigma_{b}$","$\\Chi^2/n_{dof}$"]
        runden = ["",".5f",".5f",".2f",".2f",".2f"]
        caption = ["Tabelle mit den Werten der Anpassungen für die Energie-Kanal-Kalibration"]
        labelLatex = ["energieKanalKalibration"]
        latextabelle(
                    anpassungenNamenLatex, faktor, faktorError, offset, offsetError, chiq_ndof_kalibration,
                    headline=headline,
                    runden=runden,
                    caption=caption,
                    label=labelLatex
                    )

        print("\n"+"+"*50)
        if not Set.withEu4Peak:
            print("Achtung! Eu 4 Peak wird aus den werten ausgelassen, ")
        print("Achtung! Na 1 Peak wird aus den werten ausgelassen, ")
        print("+"*50+"\n" +22*"+" + " ENDE "+22*"+" + "\n"+ "+"*50+2*"\n")


    def energyToChannel(x, mode =wahlAnpassung):
        # x sind die Energien!
        if str(mode).lower() == "normal" or mode == 0:
            mode = 0
        elif str(mode).lower() == "small" or mode == 1:
            mode = 1
        elif str(mode).lower() == "big" or mode == 2:
            mode = 2
        else: 
            print("nothingFits!!")
            mode = wahlAnpassung
        return np.array(x)*faktor[mode] + offset[mode]

    def channelToEnergy(x,mode=wahlAnpassung):
        # x sind die Channels
        if str(mode).lower() == "normal" or mode == 0:
            mode = 0
        elif str(mode).lower() == "small" or mode == 1:
            mode = 1
        elif str(mode).lower() == "big" or mode == 2:
            mode = 2
        else: 
            print("nothing matched!!!")
            mode = wahlAnpassung

        return (np.array(x) - offset[mode])/faktor[mode]

    def channelToEnergyError(x, ex, mode=wahlAnpassung):
        # x sind die Channel
        # ex sind die Unsicherheiten auf den Channel
        if str(mode).lower() == "normal" or mode == 0:
            mode = 0 
        elif  str(mode).lower() == "big" or mode == 1:
            mode = 1
        elif str(mode).lower() == "small" or mode == 2:
            mode = 2
        else: 
            print("nothingFits!!")
            mode = wahlAnpassung
        return np.sqrt((1/faktor[mode]*np.array(ex))**2+(1/faktor[mode]*offsetError[mode])**2+((np.array(x)-offset[mode])/faktor[mode]**2*faktorError[mode])**2)

    def deltaChannelToEnergy(deltaX, mode=wahlAnpassung):
        # deltaX sind die Channelbreiten
        if str(mode).lower() =="normal" or mode == 0:
            mode = 0
        elif str(mode).lower() =="big" or mode == 1:
            mode = 1
        elif str(mode).lower() == "small" or mode ==2:
            mode = 2
        else: mode=wahlAnpassung

        return np.array(deltaX)/faktor[mode]

    def deltaChannelToEnergyError(deltaX, deltaEx, mode=wahlAnpassung):
        # deltaX sind die Channelbreiten
        # deltaEx sind die Unsicherheiten auf die Channelbreiten
        if str(mode).lower() =="normal" or mode == 0:
            mode = 0
        elif str(mode).lower() =="big" or mode == 1:
            mode = 1
        elif str(mode).lower() == "small" or mode ==2:
            mode = 2
        else: mode=wahlAnpassung

        return np.sqrt((1/faktor[mode]*np.array(deltaEx))**2 + (np.array(deltaX)/faktor[mode]**2*faktorError[mode] )**2)

    # plots zur Argumentation unserer Kalibrationskurve
    if Set.plots_allowed:
        if Set.plotsEnergieChannelAnpassung :
            plt.figure(figsize=(15,7))
            plt.grid()
            plt.title("Residuenplot lineare Anpassung zwischen Channel und Energie")
            plt.xlabel("Energie der Photonen in keV")
            plt.ylabel("(Channel - Fit)")
            
            for i in range(len(dateinamenNah)):
                for j in range(len(messungenDirekt[i].anpassungen)):
                    if Set.direktChannelTrennung:
                        shift = len(dateinamenNah)
                    else:  shift = 0
                    x = [messungenDirekt[i].Probe.peakEnergy[j], messungenDirekt[i + shift].Probe.peakEnergy[j]]
                    y = np.array([messungenDirekt[i].anpassungen[j].output.beta[0], messungenDirekt[i + shift].anpassungen[j].output.beta[0]]) - energyToChannel(x,"normal")
                    ey = [np.sqrt(messungenDirekt[i].anpassungen[j].output.cov_beta[0][0]),np.sqrt(messungenDirekt[i + shift].anpassungen[j].output.cov_beta[0][0])]
                    plt.errorbar(x,y, yerr=ey,fmt=".",label=messungenDirekt[i].Probe.name +" Peak: " +str(j+1))
            
            labels = ["Fit an alle Datenpunkte","Fit bis Channel Nummer: "+str(grenzChannel),"Fit von Channel Nummer: "+str(grenzChannel)]
            for k in range(len(nameAnpassung)):
                plt.plot(energyDataLiteratur[0], np.array(energyDataLiteratur[0])*faktor[k] + offset[k] -energyToChannel(energyDataLiteratur[0],"normal"),label=labels[k], alpha=0.5)
            
            if Set.eigeneDaten:
                plt.ylim(-30,26)
            else:
                plt.ylim(-10,10)

            plt.legend()
            if Set.saveOrShow:
                plt.savefig("../plots/aaaResidumChannelEnergie")
            else: plt.show()

        if Set.plotsEnergieChannelAnpassung:
            plt.figure(figsize=(15,7))
            plt.grid()
            plt.title("Lineare Anpassung zwischen Channel und Energie")
            plt.xlabel("Energie der Photonen in keV")
            plt.ylabel("Channel")
            for i in range(len(dateinamenNah)):
                for j in range(len(messungenDirekt[i].anpassungen)):
                    if Set.eigeneDaten:
                        shift = len(dateinamenNah)
                    else:  shift = 0
                    x = [messungenDirekt[i].Probe.peakEnergy[j], messungenDirekt[i + shift].Probe.peakEnergy[j]]
                    y = np.array([messungenDirekt[i].anpassungen[j].output.beta[0], messungenDirekt[i + shift].anpassungen[j].output.beta[0]])
                    ey = [np.sqrt(messungenDirekt[i].anpassungen[j].output.cov_beta[0][0]),np.sqrt(messungenDirekt[i + shift].anpassungen[j].output.cov_beta[0][0])]
                    plt.errorbar(x,y, yerr=ey,fmt=".",label=messungenDirekt[i].Probe.name +" Peak: " +str(j+1))
            
            labels = ["Fit an alle Datenpunkte","Fit bis Channel Nummer: "+str(grenzChannel),"Fit von Channel Nummer: "+str(grenzChannel)]
            for k in range(len(nameAnpassung)):
                plt.plot(energyDataLiteratur[0], np.array(energyDataLiteratur[0])*faktor[k] + offset[k],label=labels[k], alpha=0.5)
            plt.hlines(grenzChannel,0,1400,label="Cutoff Kanal")
            plt.legend()
            if Set.saveOrShow:
                plt.savefig("../plots/aaaAnpassungChannelEnergie")
            else: plt.show()



        #Plotten der anpassungen und der literatur werte 
        if Set.plotsEnergieLiteratur:
            if Set.direktChannelTrennung: 
                modes = ["small"]
            else: modes = ["normal"]
            
            for i in range(len(messungenDirekt)):
                plt.figure(figsize=(20,10))
                plt.grid()
                plt.title("Energie Spektrum von " + messungenDirekt[i].parameter)
                plt.xlabel("Channels")
                plt.ylabel("Ereignisse pro Sekunde in 1/s")
                plt.plot(np.arange(len(messungenDirekt[i].messreihe)),messungenDirekt[i].messreihe)
                plt.plot(np.arange(len(messungenDirekt[i].messreihe)),messungenDirekt[i].messreiheRaw/messungenDirekt[i].liveTime)
                plt.plot(np.arange(len(messungenDirekt[i].messreihe)),messungenDirekt[i].messreiheBackground/messungenDirekt[i].liveTimeBackground)
                
                colors = [["green","darkred","black","darkblue","sienna","green"],["limegreen","red","dimgray","blue","chocolate","limegreen"]]
                for j,x in enumerate(messungenDirekt[i].anpassungen):
                    plt.vlines(x.output.beta[0],min(messungenDirekt[i].messreihe),max(messungenDirekt[i].messreihe), color="grey")
                    for k, mode in enumerate(modes):
                        plt.vlines(energyToChannel(messungenDirekt[i].Probe.peakEnergy[j],mode),min(messungenDirekt[i].messreihe),max(messungenDirekt[i].messreihe), color=colors[k][j])
                
                
                if Set.saveOrShow:
                    plt.savefig(base + "/plots/aa"+ messungenDirekt[i].typ + messungenDirekt[i].parameter + mode )    
                else: plt.show()

    ## (3) ∆E und Koeffizienten a und b
    
    channelData = channelData[wahlAnpassung]
    channelWidth = channelWidth[wahlAnpassung] # Halbwertsbreite des Channels
    channelWidthError = channelWidthError[wahlAnpassung]
    channelDataError = channelDataError[wahlAnpassung]

    energy = channelToEnergy(channelData)
    energyError = channelToEnergyError(channelData,channelDataError)
    deltaEnergy = (np.array(channelWidth)/faktor[wahlAnpassung])
    deltaEnergyError = deltaChannelToEnergyError(channelWidth,channelWidthError)
    

    for i in range(len(energy)):
        if Set.printDeltasInput:
            print(source[wahlAnpassung][i])
            print("∆C: {0:.3f} ± {1:.3f}".format(channelWidth[i],channelWidthError[i]))
            print("∆E: {0:.3f} ± {1:.3f}".format(deltaEnergy[i],deltaEnergyError[i]), "\n")
    
    if Set.latexTabelleDeltaEs:
        print("+"*50+"\n" + "LATEX TABELLE: ∆Es" + "\n"+ "+"*50+ "\n")
        headline = ["Peak","$\\Delta C$","$\\sigma_{\\Delta C}$","$\\Delta E$ in [$\\si{keV}$]","$\\sigma_{\\Delta E}$ in [$\\si{keV}$]"]
        runden = ["",".3f",".3f",".2f",".2f",]
        caption = ["Tabelle mit den verwendeten $\\Delta E$ und deren Unsicherheiten zur Bestimmung der Auflösung."]
        labelLatex = ["deltaE"]
        latextabelle(
                    source[wahlAnpassung],channelWidth,channelWidthError,deltaEnergy,deltaEnergyError,
                    headline=headline,
                    runden=runden,
                    caption=caption,
                    label=labelLatex
                    )

        print("\n"+"+"*50)
        print("Achtung! keine Trennung zwischen Nah und Fern")
        print("+"*50+"\n" +22*"+" + " ENDE "+22*"+" + "\n"+ "+"*50+2*"\n")


    if Set.plotDeltaChannel:
        plt.figure(figsize=(20,10)) 
        plt.title("Halbwertskanalbreite gegen Kanal aufgetragen")
        plt.grid()
        plt.xlabel("Kanal")
        plt.ylabel("Halbwertskanalbreite")
        plt.errorbar(channelData,channelWidth,xerr=channelDataError,yerr=channelDataError,fmt=".")
        if Set.saveOrShow:
            plt.savefig(base + "/plots/deltaCgegenC")
        else: plt.show()
        
        plt.figure(figsize=(20,10)) 
        plt.grid()
        plt.title("Halbwertsenergiebreite gegen Energie aufgetragen")
        plt.xlabel("Energie [keV]")
        plt.ylabel("Halbwertsenergiebreite [keV]")
        plt.errorbar(energy,deltaEnergy,xerr=energyError,yerr=deltaEnergyError,fmt=".")
        if Set.saveOrShow:
            plt.savefig(base + "/plots/deltaEgegenE")
        else: plt.show()


    ey = np.sqrt((2*deltaEnergy/energy*deltaEnergyError)**2+((deltaEnergy/energy)**2*energyError)**2)

    #(∆E**2/E) = a**2/E + b**2
    anpassungDeltaE = linAnpassung(energy, (deltaEnergy**2/energy), exValues=energyError, eyValues=ey ,plot=Set.plotLinDeltaDirket,title="∆E^2/E zur Bestimmung von a und b", ).output
    
    if Set.printDeltasOutput:
        print("a: ", np.sqrt(anpassungDeltaE.beta[0]))
        print("ea:", np.sqrt(np.sqrt(anpassungDeltaE.cov_beta[0][0])),"\n")
        print("b: ", np.sqrt(anpassungDeltaE.beta[1]))
        print("eb:", np.sqrt(np.sqrt(anpassungDeltaE.cov_beta[1][1])),"\n")

    if Set.latexTabelleEnergieaufloesung:
        print("+"*50+"\n" + "LATEX TABELLE: Energieaufloesung" + "\n"+ "+"*50+ "\n")
        headline = ["$a$","$\\sigma_{a}$","$b$","$\\sigma_{b}$","$\\Chi^2/n_{dof}$"]
        runden = [".4f",".4f",".2f",".2f",".2f"]
        caption = ["Tabelle mit den bestimmten Parametern der Auflösung."]
        labelLatex = ["energieAufloesung"]
        latextabelle(
                    [np.sqrt(anpassungDeltaE.beta[0])],[np.sqrt(np.sqrt(anpassungDeltaE.cov_beta[0][0]))],[np.sqrt(anpassungDeltaE.beta[1])],[np.sqrt(np.sqrt(anpassungDeltaE.cov_beta[1][1]))],[anpassungDeltaE.res_var],
                    headline=headline,
                    runden=runden,
                    caption=caption,
                    label=labelLatex
                    )

        #print("\n"+"+"*50)
        #print("Achtung! keine Trennung zwischen Nah und Fern")
        print("+"*50+"\n" +22*"+" + " ENDE "+22*"+" + "\n"+ "+"*50+2*"\n")

    ## (4) Effizienz
    if Set.printOutputEffizenz:
        print("\n"+"-"*20 + "  EFFIZENZ  " + "-"*20)
    
    x, y, ex, ey = [],[],[],[]
    allHeights = []
    allHeightsError = []
    allHeightsSource = []
    allAktivity = []
    allAktivityError = []
    allInensity = []

    for i,m in enumerate(messungenDirekt):
        height = []
        heightError = []
        heightSource = []
        for j,a in enumerate(m.anpassungen):
            out = a.output
            if out.beta[0] < grenzChannel:
                height += [out.beta[2]]
                heightError += [np.sqrt(out.cov_beta[2][2])]
                heightSource += ["{0} Peak: {1}".format(m.Probe.name,j+1)]
                allAktivity += [m.Probe.a]
                allAktivityError += [m.Probe.ae]
                allInensity += [m.Probe.peaksIntensity[j]]

        tmp = effizienz(
                messungenDirekt[i].Probe,  
                height,
                heightError,
                nah = (nahOrFernIndex(i)==0),
                )
        messungenDirekt[i].effizenz = tmp[0]
        messungenDirekt[i].err_effizenz = list(map(float,tmp[1]))

        allHeights += height
        allHeightsError += heightError
        allHeightsSource += heightSource


        x += list(channelToEnergy([messungenDirekt[i].anpassungen[j].output.beta[0] for j in range(len(messungenDirekt[i].anpassungen)) if messungenDirekt[i].anpassungen[j].output.beta[0] < grenzChannel]))
        ex += list(channelToEnergyError(
            [messungenDirekt[i].anpassungen[j].output.beta[0] for j in range(len(messungenDirekt[i].anpassungen)) if messungenDirekt[i].anpassungen[j].output.beta[0] < grenzChannel],
            [np.sqrt(messungenDirekt[i].anpassungen[j].output.cov_beta[0][0]) for j in range(len(messungenDirekt[i].anpassungen)) if messungenDirekt[i].anpassungen[j].output.beta[0] < grenzChannel])
            )
        y += list(messungenDirekt[i].effizenz)
        ey += list(messungenDirekt[i].err_effizenz)

    anpassungEffizenz = linAnpassung(x,np.array(y)*100,exValues=ex, eyValues=np.array(ey)*100)

    if Set.latexTabellePeakHeight:
        print("+"*50+"\n" + "LATEX TABELLE: Input Effizenz" + "\n"+ "+"*50+ "\n")
        headline = ["Peak","Ereignisrate des Peaks [$1/\\si{s}$]","Unicherheit auf die Ereignisrate des Peaks [$1/\\si{s}$]","Aktivität: $A$ [$1/\\si{s}$]","$\\sigma_{A}$ [$1/\\si{s}$]","Intensität: $I$ [%]"]
        runden = ["", ".2f",".2f",".2f",".2f", ""]
        caption = "Die Ereignisrate der Peakhöhepunkte und die Unsicherheit auf die selben zur Bestimmung der Effizenz"
        labelLatex = "ereignisrate"
        latextabelle(
                    allHeightsSource,
                    allHeights,
                    allHeightsError,
                    allAktivity,
                    allAktivityError,
                    allInensity,
                    headline=headline,
                    runden=runden,
                    caption=caption,
                    label=labelLatex
                    )

        print("\n"+"+"*50)
        print("Achtung! keine Trennung zwischen Nah und Fern")
        print("+"*50+"\n" +22*"+" + " ENDE "+22*"+" + "\n"+ "+"*50+2*"\n")


    if Set.latexTabelleInensity:
        print("+"*50+"\n" + "LATEX TABELLE: Effizenzen" + "\n"+ "+"*50+ "\n")
        headline = ["Peak","$E$ [$\\si{keV}$]","$\\sigma_{E}$ [$\\si{keV}$]","Effizenz: $\\epsilion$ [%]"," $\\sigma_{\\epsilion$} [%]"]
        runden = ["",".2f",".2f",".2f",".2f"]
        caption = "Tabelle mit den bestimmten Effizenzen und deren Unsicherheiten"
        labelLatex = "effizenz"
        latextabelle(
                    allHeightsSource,
                    x, ex,
                    np.array(y)*100, np.array(ey)*100,
                    headline=headline,
                    runden=runden,
                    caption=caption,
                    label=labelLatex
                    )

        print("\n"+"+"*50)
        print("!RUNDEN!")
        print("Achtung! keine Trennung zwischen Nah und Fern")
        print("+"*50+"\n" +22*"+" + " ENDE "+22*"+" + "\n"+ "+"*50+2*"\n")

    if Set.plotOutputEffizenz:
        plt.figure(figsize=(20,10))
        plt.grid()
        plt.title("Effizenz gegen Energie")
        plt.xlabel("Energie [keV]")
        plt.ylabel("Effizenz [%]")
        plt.plot(np.linspace(min(x),max(x)), anpassungEffizenz.output.beta[0]*np.linspace(min(x),max(x)) + anpassungEffizenz.output.beta[1])
        #plt.errorbar(x, np.array(y)*100,fmt=".",xerr=ex, yerr=np.array(ey)*100)
        for i in range(len(x)):
            plt.errorbar(x[i], np.array(y[i])*100,fmt=".",xerr=ex[i], yerr=np.array(ey[i])*100,label=allHeightsSource[i])
        
        plt.legend()
        if Set.saveOrShow:
            plt.savefig(base + "/plots/effizenz")
        else: plt.show()

        


    ## (5) m_e bestimmen
    m_e_lit = 510.9989461 
    if Set.printMasseElektron:
        for i in indexNa1[wahlAnpassung]:
            print("Energy/Masse Elektron: {0} ± {1}\n".format(energy[i],energyError[i]))
            # Besonders gut geeignet, da durch annhilation Photonen mit der Energie der Masse von Elektronen unterwegs sind


    if Set.latexMasseElektron:
        print("+"*50+"\n" + "LATEX TABELLE: Masse Elektron Na " + "\n"+ "+"*50+ "\n")
        headline = ["$m_e$","$\\sigma_{m_e}$","Vergleich Literatur"]
        runden = [".2f",".2f",".2f"]
        caption = "Masse der Elektronen in natürlichen Einheiten bestimmt durch den ersten Na-22 Peak. Aus naher und ferner Distanz."
        labelLatex = "masseElektronNa22"
        latextabelle(
                    [energy[i] for i in indexNa1[wahlAnpassung]], 
                    [energyError[i] for i in indexNa1[wahlAnpassung]],
                    [(energy[i]-m_e_lit)/energyError[i] for i in indexNa1[wahlAnpassung]],
                    headline=headline,
                    runden=runden,
                    caption=caption,
                    label=labelLatex
                    )

        print("\n"+"+"*50)
        print("Achtung! keine Trennung zwischen Nah und Fern")
        print("+"*50+"\n" +22*"+" + " ENDE "+22*"+" + "\n"+ "+"*50+2*"\n")

###################################
###### ENDE DIREKT AUSWERUNG ######
###################################



### Auswertung Konventionell ###
if Set.konventionell:
    messungenFe = []
    messungenAl = []
    channelEnergyPeakFe = []
    channelEnergyPeakAl = []
    for i in range(len(winkelKonventionell)):
        if Set.printEinlesen:
            print("Messung Konventionell:", i+1, "[",winkelKonventionell[i],"]")
            print(dateinamenKonventionellAl[i],"\n")

        # Eisen
        messungenFe.append(Messung(dateinamenKonventionellFe[i], dateinamenBackgroundKonventionell[i], "konventionellFe", winkelKonventionell[i],"Cs173s"))
        #Gauss Anpassung an die Peaks 
        messungenFe[i].analyse = gaussAnpassung(messungenFe[i], intervallPeakArrayFe[i], Set.plotGaussKonventionell, "KonventionellFe" + winkelKonventionell[i])

        #Aluminium
        messungenAl.append(Messung(dateinamenKonventionellAl[i], dateinamenBackgroundKonventionell[i], "konventionellAl", winkelKonventionell[i],"Cs173s"))
        #Gauss Anpassung an die Peaks 
        messungenAl[i].analyse = gaussAnpassung(messungenAl[i], intervallPeakArrayAl[i], Set.plotGaussKonventionell, "KonventionellFe" + winkelKonventionell[i])
        
        
    #Plots der Messreihen
    if Set.plotKonventionellGesamt:
        MessungenStreuBody = [messungenAl, messungenFe]
        namen = ["messungenAl", "messungenFe"]
        peakintervalle = [intervallPeakArrayAl, intervallPeakArrayFe]
        for k in range(len(MessungenStreuBody)):
            for i in range(len(winkelZyklisch)):
                plt.figure(figsize=(20,10))
                plt.title("Übersicht konventionell "+winkelKonventionell[i]+" Grad " + namen[k])
                plt.xlabel("Channel")
                plt.ylabel("Häufigkeit [1/s]")
                plt.grid()
                plt.errorbar(
                    np.arange(len(MessungenStreuBody[k][i].messreihe)),
                    MessungenStreuBody[k][i].messreihe,
                    label= winkelKonventionell[i] + "°")
                plt.errorbar(
                    np.arange(len(MessungenStreuBody[k][i].messreiheRaw)),
                    MessungenStreuBody[k][i].messreiheRaw/MessungenStreuBody[k][i].liveTime,
                    label= winkelKonventionell[i] + "° (Rohdaten)")
                plt.errorbar(
                    np.arange(len(MessungenStreuBody[k][i].messreiheBackground)),
                    MessungenStreuBody[k][i].messreiheBackground/MessungenStreuBody[k][i].liveTimeBackground,
                    label= winkelKonventionell[i] + "° (Hintergrund)")
                plt.plot(np.arange(len(MessungenStreuBody[k][i].messreiheBackground)),gf(MessungenStreuBody[k][i].analyse.output.beta,np.arange(len(MessungenStreuBody[k][i].messreiheBackground))))
                plt.vlines(peakintervalle[k][i],min(MessungenStreuBody[k][i].messreihe),max(MessungenStreuBody[k][i].messreihe),)
                plt.legend()
                plt.legend()
                plt.vlines(MessungenStreuBody[k][i].analyse.output.beta[0],min(MessungenStreuBody[k][i].messreihe),max(MessungenStreuBody[k][i].messreihe), color="red")
                if Set.saveOrShow:
                    plt.savefig(base + "/plots/messungenkonventionell"+winkelKonventionell[i]+"Grad"+namen[k])
                else: plt.show()








### Auswertung Zyklisch
if Set.zyklisch:    
    messungenZyklisch = []
    for i in range(len(winkelZyklisch)):   
        if Set.printEinlesen:
            print("Messung zyklisch:", i+1, "[",winkelZyklisch[i],"]")
            print(dateinamenKonventionellAl[i],"\n") 
        #einlesen und definieren der Messreihen mit Korrektur Offset
        messungenZyklisch.append(Messung(dateinamenZirkular[i], dateinamenBackgroundZirkular[i],"zirkular", winkelZyklisch[i],"Cs173s"))
        
        #Gauss Anpassung an die Peaks 
        messungenZyklisch[i].analyse = gaussAnpassung(messungenZyklisch[i], intervallPeakArray[i], Set.plotGaussZyklisch )
        #messungZyklisch[i].analyse.output.beta[x] speichert die ergebnisse (x=0 Mittelwert, x=1, Breite)

        #Plotten aller Rohdaten
        #messungenZyklisch[i].plotData("Zirkular: " + winkelZyklisch[i] ,"zirkular"+ winkelZyklisch[i]+ "Gradd")

    #Plots der Messreihen
    if Set.plotZyklischGesamt:
        for i in range(len(winkelZyklisch)):
            plt.figure(figsize=(20,10))
            plt.title("Übersicht zyklisch")
            plt.grid()
            plt.errorbar(
                np.arange(len(messungenZyklisch[i].messreihe)),
                messungenZyklisch[i].messreihe,
                label= winkelZyklisch[i] + "°")
            plt.vlines(intervallPeakArray[i],0,1)
            plt.legend()
            plt.legend()
            plt.vlines(messungenZyklisch[i].analyse.output.beta[0],0,1, color="red")
            if Set.saveOrShow:
                plt.savefig(base + "/plots/messungenZyklisch"+winkelZyklisch[i]+"Grad" )
            else: plt.show()



#Auswertung Compton-Streuung
#Plot Zusammenhang Winkel und Energie
## (2.2) Winkel Energie Anhänigkeit
winkelError = 2 #in Grad
if Set.plotWinkelPeakEnergy and Set.zyklisch and Set.konventionell:
    plt.figure(figsize=(20,10))
    plt.title("Energie gegen Winkel von Compton Streuung")
    plt.grid()
    plt.xlabel("Winkel [°]")
    plt.ylabel("Energie [keV]")
    for i in range(len(winkelZyklisch)):
        energyPeakZykl = channelToEnergy(messungenZyklisch[i].analyse.output.beta[0])
        energyPeakZyklError = channelToEnergyError(messungenZyklisch[i].analyse.output.beta[0], np.sqrt(messungenZyklisch[i].analyse.output.cov_beta[0][0]))
        plt.errorbar(int(winkelZyklisch[i]), energyPeakZykl, yerr=energyPeakZyklError, xerr=winkelError, fmt=".",label="Kreis: " + str(winkelZyklisch[i]) + "°")

    for i in range(len(winkelKonventionell)):

        energyPeakAl = channelToEnergy(messungenAl[i].analyse.output.beta[0])
        energyPeakAlError = channelToEnergyError(messungenAl[i].analyse.output.beta[0], np.sqrt(messungenAl[i].analyse.output.cov_beta[0][0]))
        energyPeakFe = channelToEnergy(messungenFe[i].analyse.output.beta[0])
        energyPeakFeError = channelToEnergyError(messungenFe[i].analyse.output.beta[0], np.sqrt(messungenFe[i].analyse.output.cov_beta[0][0]))

        plt.errorbar(int(winkelKonventionell[i]), energyPeakAl,yerr=energyPeakAlError, xerr=winkelError ,fmt=".", label='Al '+str(winkelKonventionell[i])+"°")
        plt.errorbar(int(winkelKonventionell[i]), energyPeakFe,yerr=energyPeakFeError, xerr=winkelError ,fmt=".", label='Fe '+str(winkelKonventionell[i])+"°")

    plt.plot(np.linspace(0, 140), energyWinkelKurve(661.66, np.linspace(0, 140)), color='black', label='Theorie')
    plt.legend()
    if Set.saveOrShow:
        plt.savefig("../plots/energieGegenWinkel")
    else: plt.show()


#Einsammeln der Peaks inkl. zugehöriger Winkel
if Set.auswertungCompton and Set.zyklisch and Set.konventionell:

    Peaks = []
    Peaks_err = []
    winkel = []

    for i in range(len(winkelKonventionell)):

        Peaks += [channelToEnergy(messungenAl[i].analyse.output.beta[0]), 
                  channelToEnergy(messungenFe[i].analyse.output.beta[0]), 
                  channelToEnergy(messungenZyklisch[i].analyse.output.beta[0])]
        Peaks_err += [channelToEnergyError(messungenAl[i].analyse.output.beta[0], np.sqrt(messungenAl[i].analyse.output.cov_beta[0][0])),
                      channelToEnergyError(messungenFe[i].analyse.output.beta[0], np.sqrt(messungenFe[i].analyse.output.cov_beta[0][0])), 
                      channelToEnergyError(messungenZyklisch[i].analyse.output.beta[0], np.sqrt(messungenZyklisch[i].analyse.output.cov_beta[0][0]))]
        winkel  += [messungenAl[i].parameter,
                    messungenFe[i].parameter,
                    messungenZyklisch[i].parameter]
    
    winkel = list(map(int, winkel))
    winkel = np.deg2rad(np.array(winkel))
    
    Peaks = np.array(Peaks)
    Peaks_err = np.array(Peaks_err)

    E = Probe("Cs137s").peakEnergy[0]

#(2.3) Wirkungsquerschnitt 
if Set.konventionell and Set.zyklisch and Set.auswertungCompton:

    #lange Version:..........................((((((((((((((((((
    #Effizienz Cs137s 
    epsilon1 = messungenDirekt[1].effizenz[0]
    sigma_ep1 = messungenDirekt[1].err_effizenz[0]
    epsilon2 = messungenDirekt[5].effizenz[0]
    sigma_ep2 = messungenDirekt[5].err_effizenz[0]

    mean_epsilon = (epsilon1/sigma_ep1**2+epsilon2/sigma_ep2**2)/(1/sigma_ep1**2+1/sigma_ep2**2)
    sigma_epsilon = 1/(1/sigma_ep1**2+1/sigma_ep2**2)*np.sqrt(1/sigma_ep1**2+1/sigma_ep2**2)

    #ms einsammeln
    ms = []
    err_ms = []
    colors = []
    for i in range(len(messungenAl)):
        ms += [messungenAl[i].analyse.output.beta[2],
                messungenFe[i].analyse.output.beta[2],
                messungenZyklisch[i].analyse.output.beta[2]] #Maximale Ereignisse pro Sekunde
        err_ms += [messungenAl[i].analyse.output.cov_beta[2][2],
                   messungenFe[i].analyse.output.cov_beta[2][2],
                   messungenZyklisch[i].analyse.output.cov_beta[2][2]]# Fehler auf das selbige
        colors += ['red', 'blue', 'green']

    wirkung = []
    std_wirkung = []
    for i in range(len(Peaks)):
        wirkung += [wirkungsquerschnitt(Probe("Cs137s"), Peaks[i], mean_epsilon, sigma_epsilon, ms[i], err_ms[i])[0]]
        std_wirkung += [wirkungsquerschnitt(Probe("Cs137s"), Peaks[i], mean_epsilon, sigma_epsilon, ms[i], err_ms[i])[0]]

    if Set.plotWirkungsquerschnitt:        

        plt.figure(figsize=(20,10))
        plt.title('Wirkungsquerschnitt')

        for i in range(len(winkel)):
            plt.errorbar(winkel[i], wirkung[i], xerr=np.deg2rad(winkelError),fmt='.', color=colors[i], label='Messwerte')
        #plt.plot(np.linspace(0, np.pi), kleinNishaFormel(E, energyWinkelKurve(E, np.linspace(0, 180)), np.linspace(0, np.pi)), color='black', label='Theorie')
        
        plt.legend()
        if Set.saveOrShow:
            plt.savefig("../plots/wirkungsquerschnitt")        
        else:
            plt.show()



    #Klein-Nishina
    y_err = []
    for i in range(len(winkel)):
        y_err += [err_kleinNishina(E, Peaks[i], winkel[i], 0, Peaks_err[i], float(np.deg2rad(2)))]
    
    if Set.plotWirkungsquerschnitt:        

        plt.figure(figsize=(20,10))
        plt.title('Wirkungsquerschnitt')

        plt.errorbar(winkel, kleinNishaFormel(E, Peaks, winkel), xerr=np.deg2rad(winkelError), fmt='.', color='blue', label='Messwerte')
        plt.plot(np.linspace(0, np.pi), kleinNishaFormel(E, energyWinkelKurve(E, np.linspace(0, 180)), np.linspace(0, np.pi)), color='black', label='Theorie')
        
        plt.legend()
        if Set.saveOrShow:
            plt.savefig("../plots/wirkungsquerschnitt")        
        else:
            plt.show()



#(2.4) Bestimmung der Elektronenmasse aus der Compton-Streuung
if Set.auswertungCompton and Set.konventionell and Set.zyklisch:

    err_winkel = len(winkel)*[np.deg2rad(2)]
    err_x = []
    sigma_winkel = np.deg2rad(2)
    for i in winkel:
        err_x += [np.sin(i)*sigma_winkel]
    
    err_y = []
    for i in range(len(Peaks_err)):
        err_y += [1/(Peaks[i])**2*Peaks_err[i]]
    

    linreg = linAnpassung(1-np.cos(winkel), 1/Peaks-1/E, exValues=err_x, eyValues=err_y, title="Elektronenmasse aus Compton-Streuung", plot=Set.plotM_eCompton).output

    m_e = 1/(linreg.beta[0])




print("\n"+"*"*30+" ENDE " + "*"*30)