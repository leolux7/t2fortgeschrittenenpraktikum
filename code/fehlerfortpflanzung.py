# -*- coding: utf-8 -*-

import sympy as sp 
from sympy.printing.latex import print_latex
from scipy.constants import c, e,m_e, h, pi

##########################
# Unsicherheit auf Gauss #
##########################
delx, sigma, err_sigma = sp.symbols("delx, sigma, err_sigma")

gauss = 1/sp.sqrt(2*sp.pi*sigma**2)*sp.exp(-delx**2/(2*sigma**2))
equ = sp.Eq(gauss, 1/2)
Deltax = sp.solve(equ, delx)[1]

#Fehlerfortpflanzung
ddx_dsigma = sp.diff(Deltax, sigma)

err = ddx_dsigma*err_sigma

def err_gauss(sigma0, err_sigma0, latex=False):
    if latex == True:
        print_latex(err)
    
    err0 = err.subs([(sigma, sigma0), (err_sigma, err_sigma0)])
    
    return err0.evalf()


#############################
# Unsicherheit auf Effizenz #
#############################
r, A, I, m, F_D, err_r, err_A, err_I, err_m, err_F_D = sp.symbols('r, A, I, m, F_D, err_r, err_A, err_I, err_m, err_F_D')
    
epsilon = 4*sp.pi*r**2/(A*I)*m/F_D

#Fehlerforpflanzung
depsilon_dr = sp.diff(epsilon, r)
depsilon_dA = sp.diff(epsilon, A)
depsilon_dI = sp.diff(epsilon, I)
depsilon_dm = sp.diff(epsilon, m)
depsilon_dF_D = sp.diff(epsilon, F_D)

epsilon_err = (depsilon_dr*err_r)**2+(depsilon_dA*err_A)**2+(depsilon_dI*err_I)**2+(depsilon_dI*err_I)**2+(depsilon_dm*err_m)**2+(depsilon_dF_D*err_F_D)**2
epsilon_err = sp.sqrt(epsilon_err)


def err_epsilon(r0, err_r0, A0, err_A0, I0, err_I0, m0, err_m0, F_D0, err_F_D0, latex=False):
    if latex == True:
        print_latex(epsilon_err)
        
    error = epsilon_err.subs([(r,r0), (err_r,err_r0), (A,A0), (err_A, err_A0), (I,I0), (err_I,err_I0), (m, m0), (err_m, err_m0), (F_D,F_D0), (err_F_D,err_F_D0)])
    
    return error.evalf()

#######################
# Wirkungsquerschnitt #
#######################

#Absorption
mu_Luft, mu_Luft_nach, mu, mu_nach, r0_Luft, r0_Luft_err, r0, r0_err, r_Luft, r_Luft_err, r, r_err = sp.symbols("mu_Luft, mu_Luft_nach, mu, mu_nach, r0_Luft, r0_Luft_err, r0, r0_err, r_Luft, r_Luft_err, r, r_err")

n = sp.exp(-mu_Luft*r0_Luft)*sp.exp(-mu*r0)*sp.exp(-mu_nach*r)*sp.exp(-mu_Luft_nach*r_Luft)

dn_dr0_Luft     =   sp.diff(n, r0_Luft)
dn_dr_Luft      =   sp.diff(n, r_Luft) 
dn_dr0          =   sp.diff(n, r0)
dn_dr           =   sp.diff(n, r)

n_error = sp.sqrt((dn_dr0_Luft*r0_Luft_err)**2+(dn_dr_Luft*r_Luft_err)**2+(dn_dr0*r0_err)**2+(dn_dr*r_err)**2)

def absorption_err(mu0_Luft, mu0_Luft_nach, mu0, mu0_nach, r_0_Luft, r_0_Luft_err, r_0, r_0_err, rLuft, rLuft_err, r_, rerr, latex=False):
    
    if latex:
        sp.print_latex(n_error)

    n_err0 = n_error.subs([(mu_Luft, mu0_Luft), (mu_Luft_nach, mu0_Luft_nach), (mu, mu0), (mu_nach, mu0_nach), (r0_Luft, r_0_Luft), (r0_Luft_err, r_0_Luft_err), (r0, r_0), (r0_err, r_0_err), (r_Luft, rLuft), (r_Luft_err, rLuft_err), (r, r_), (r_err, rerr)])
    return n_err0.evalf()

#Elektronen im Streuvolumen
r, r_err, h, h_err, rho, m, z = sp.symbols("r, r_err, h, h_err, rho, m, z")

N_e = z*sp.pi*r**2*h*rho/m

dNe_dh = sp.diff(N_e, h)
dNe_dr = sp.diff(N_e, r)

Ne_error = sp.sqrt((dNe_dh*h_err)**2+(dNe_dr*r_err)**2)

def N_e_err(r0, r0_err, h0, h0_err, masse, dichte,Z, latex=False):

    if latex:
        sp.print_latex(Ne_error)
    
    err_Ne = Ne_error.subs([(r, r0), (r_err, r0_err), (h, h0), (h_err, h0_err), (m, masse), (rho, dichte), (z, Z)])
    return err_Ne.evalf()

m, m_err, A, A_err, I, I_err, epsilon, std_epsilon, N_e, Ne_err, F_D, F_D_err, r_err, r0, r0_err, n, n_err = sp.symbols("m, m_err, A, A_err, I, I_err, epsilon, epsilon_err, N_e, N_e_err, F_D, F_D_err, r_err, r0, r0_err, n, n_err")

#Wirkungsquerschnitt
wirkung = (r_Luft+r)**2/F_D*m/(n*epsilon*N_e)*4*sp.pi*(r0_Luft+r0)**2/(A*I)

dwirkung_dr_Luft    =   sp.diff(wirkung, r_Luft)
dwirkung_dr         =   sp.diff(wirkung, r)
dwirkung_dF_D       =   sp.diff(wirkung, F_D)
dwirkung_dm         =   sp.diff(wirkung, m)
dwirkung_dn         =   sp.diff(wirkung, n)
dwirkung_depsilon   =   sp.diff(wirkung, epsilon)
dwirkung_dN_e       =   sp.diff(wirkung, N_e)
dwirkung_dr0_Luft   =   sp.diff(wirkung, r0_Luft)
dwirkung_dr0        =   sp.diff(wirkung, r0)
dwirkung_dA         =   sp.diff(wirkung, A)
dwirkung_dI         =   sp.diff(wirkung, I)

wirkung_err = sp.sqrt((dwirkung_dr_Luft*r_Luft_err)**2+
                        (dwirkung_dr*r_err)**2+
                        (dwirkung_dF_D*F_D_err)**2+
                        (dwirkung_dm*m_err)**2+
                        (dwirkung_dn*n_err)**2+
                        (dwirkung_depsilon*std_epsilon)**2+
                        (dwirkung_dN_e*Ne_err)**2+
                        (dwirkung_dr0_Luft*r0_Luft_err)**2+
                        (dwirkung_dr0*r0_err)**2+
                        (dwirkung_dA*A_err)**2+
                        (dwirkung_dI*I_err)**2)

def err_wirkung(rLuft, rLuft_err, r_, rerr, F_D0, F_Derr, m0, m_err0, n0, n0_err, epsilon0, epsilon0_err, Ne, Ne0_err, r_0_Luft, r_0_Luft_err, r_0, r_0_err, A0, A0_err, I0, I0_err, latex=False):
    
    if latex:
        sp.print_latex(wirkung_err)

    err_wirkung = wirkung_err.subs([(r_Luft, rLuft), (r_Luft_err, rLuft_err), (r, r_), (r_err, rerr), (F_D, F_D0), (F_D_err, F_Derr), (m, m0), (m_err, m_err0), (n, n0), (n_err, n0_err), (epsilon, epsilon0), (std_epsilon, epsilon0_err), (N_e, Ne), (Ne_err, Ne0_err), (r0_Luft, r_0_Luft), (r0_Luft_err, r_0_Luft_err), (r0, r_0), (r0_err, r_0_err), (A, A0), (A_err, A0_err), (I, I0), (I_err, I0_err)])
    print(err_wirkung)
    return err_wirkung.evalf()

########################
# Klein-Nishina-Formel #
########################

theta, E, E_nach, err_E, err_E_nach, err_theta = sp.symbols("theta, E, E_nach, err_E, err_E_nach, err_theta")

roh = E/E_nach
lambda_C = h/(m_e*c)
alpha = e**2/(4*pi)

wirkung = alpha**2 * lambda_C**2 /(8*pi**2) * 1/roh**2 * (roh + 1/roh - sp.sin(theta)**2)

dwirkung_dtheta = sp.diff(wirkung, theta)
dwirkung_dE = sp.diff(wirkung, E)
dwirkung_dE_nach = sp.diff(wirkung, E_nach)

err = sp.sqrt((dwirkung_dtheta*err_theta)**2 + (dwirkung_dE*err_E)**2 + (dwirkung_dE_nach*err_E_nach)**2 )

def err_kleinNishina(E0, E_nach0, theta0, E_err, E_nach_err, theta_err,  latex=False):
    if latex==True:
        print_latex(err)

    err0 = err.subs([(E, E0),(E_nach, E_nach0 ),(theta, theta0), (err_E, E_err),(err_E_nach, E_nach_err),  (err_theta, theta_err)])

    return err0.evalf()


