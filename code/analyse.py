import matplotlib.pyplot as plt
import scipy.odr as odr
import numpy as np


from settings import Settings, fehlerfortpflanzung
from fehlerfortpflanzung import err_epsilon, absorption_err, N_e_err, err_wirkung
from scipy.constants import c
from dataClass import Probe
from scipy.constants import c, e,m_e, h, pi, physical_constants

Set = Settings()


#chiq = output.res_var

def gf(B,x):
    #B[0] ist der Erwartunswert
    #B[1] ist die Standardabweichung
    #B[2] da messwerte nicht auf 1 normiert sind 
    #B[3] offset in y richtung
    
    # B[2] * g(x,B[0]=µ, B[1]=std) + B[3]; g() is true gauss
    if Set.offset:
        return B[2]*1/np.sqrt(2*np.pi*B[1]**2) * np.exp(-1*(x-B[0])**2/(2*B[1]**2)) + B[3]
    else:
        return B[2]*1/np.sqrt(2*np.pi*B[1]**2) * np.exp(-1*(x-B[0])**2/(2*B[1]**2))
    

def lf(B,x):
    #B[0] ist die Steigung
    #B[1] ist die Verschiebung
    return B[0]*x + B[1]

def qf(B,x):
    return B[0]*x**2 + B[1]*x

gauss = odr.Model(gf)
linear = odr.Model(lf)
quadratic = odr.Model(qf)

def gaussAnpassung(Messung,channelRange, plot=False, extraTitle="", halbwert=False):

    cutMessreihe = Messung.messreihe[channelRange[0]:channelRange[1]]
    cutMessreiheError = Messung.messreiheError[channelRange[0]:channelRange[1]]
    cutChannels = np.arange(channelRange[0],channelRange[1])
    
    guess = [cutChannels[list(cutMessreihe).index(max(cutMessreihe))],(channelRange[1]-channelRange[0])/4,sum(cutMessreihe)]
    if Set.offset:
        guess.append(0)

    #guess[0] höchster messwert auf dem intervall
    #guess[1] wir nehmen an das wir mit dem auge ein 2 sigma intervall gewählt haben 
    #guess[3] wir summieren über alle bins um den linearen Faktor abzuschätzen
    
    #print(guess)


    myData = odr.RealData(cutChannels, cutMessreihe, sy=cutMessreiheError, sx=1/np.sqrt(12)) #Unsicherheit auf die Messwerte ist die Wurzel des ersten guesses
    myOdr = odr.ODR(myData, gauss, beta0=guess)
    myOdr.run()

    if halbwert:
        deltaChannel = 2*np.sqrt(2*np.log(2))* abs(myOdr.output.beta[1])
        deltaChannelError = 2*np.sqrt(2*np.log(2))* np.sqrt(myOdr.output.cov_beta[1][1])
    
    if Set.printAnpassungGaus:
        print("Anpassung Gauss:")
        print("chiq:    ",myOdr.output.res_var)
        print("µ:        {0} ± {1}".format(myOdr.output.beta[0],np.sqrt(myOdr.output.cov_beta[0][0])) )
        print("offset:  ", myOdr.output.beta[3])
        print("offset(%): ", myOdr.output.beta[3]/(myOdr.output.beta[3]+myOdr.output.beta[2]/np.sqrt(2*np.pi*myOdr.output.beta[1]**2))*100)
        #print(myOdr.output.beta)
        #myOdr.output.pprint()
        print("")

    if plot:
        addChannel = 50
        if len(Messung.messreihe) < addChannel+channelRange[1]:
            channelR = len(Messung.messreihe)
        else:
            channelR = channelRange[1]+ addChannel
        
        if 0 > channelRange[0] - addChannel:
            channelL = 0
        else:
            channelL = channelRange[0] - addChannel

        channelsPlot = np.arange(channelL, channelR)

        fig, axs = plt.subplots(2,1, sharex=True, figsize=(20,10),gridspec_kw={'height_ratios': [3,1]})
        
        dataY = [
            Messung.messreihe[channelsPlot[0]:channelsPlot[-1]+1],
            Messung.messreihe[channelsPlot[0]:channelsPlot[-1]+1]-gf(myOdr.output.beta,channelsPlot)
            ]

        for i in range(len(dataY)):
            axs[i].grid()
            axs[i].vlines(
                channelRange,
                min(dataY[i])-(max(dataY[i])-min(dataY[i]))/15,
                max(dataY[i])+(max(dataY[i])-min(dataY[i]))/15,
                color="red")

            axs[i].vlines(
                myOdr.output.beta[0],
                min(dataY[i])-(max(dataY[i])-min(dataY[i]))/15,
                max(dataY[i])+(max(dataY[i])-min(dataY[i]))/15,
                color="green")

            axs[i].errorbar(
                channelsPlot,
                dataY[i],
                yerr=Messung.messreiheError[channelsPlot[0]:channelsPlot[-1]+1],
                fmt=".")

            if halbwert:
                axs[i].vlines(
                    [myOdr.output.beta[0] - deltaChannel/2, myOdr.output.beta[0] + deltaChannel/2, ],
                    min(dataY[i])-(max(dataY[i])-min(dataY[i]))/15,
                    max(dataY[i])+(max(dataY[i])-min(dataY[i]))/15,
                    color="blue")
                axs[i].vlines(
                    [myOdr.output.beta[0] - deltaChannel/2 - deltaChannelError/2, myOdr.output.beta[0] + deltaChannel/2 - deltaChannelError/2, myOdr.output.beta[0] - deltaChannel/2 + deltaChannelError/2, myOdr.output.beta[0] + deltaChannel/2 + deltaChannelError/2,],
                    min(dataY[i])-(max(dataY[i])-min(dataY[i]))/15,
                    max(dataY[i])+(max(dataY[i])-min(dataY[i]))/15,
                    color="blue",
                    alpha=0.5)

                
        
        if halbwert:
            offset = myOdr.output.beta[3]
            axs[0].hlines( 
                [myOdr.output.beta[2]/np.sqrt(2*np.pi*myOdr.output.beta[1]**2) + offset,offset],
                min(channelsPlot),max(channelsPlot),color="gold")
            
            axs[0].hlines([0.5*myOdr.output.beta[2]/np.sqrt(2*np.pi*myOdr.output.beta[1]**2) + offset],min(channelsPlot),max(channelsPlot),color="limegreen")

       
        #Messreihe einplotten
        axs[0].errorbar(
            channelsPlot,
            Messung.messreiheRaw[channelsPlot[0]:channelsPlot[-1]+1]/ Messung.liveTime,
            yerr=Messung.messreiheRawError[channelsPlot[0]:channelsPlot[-1]+1]/ Messung.liveTime,
            fmt=".",
            alpha=0.7,
            color="lime"
            )

        #Background einplotten
        axs[0].errorbar(
            channelsPlot,
            Messung.messreiheBackground[channelsPlot[0]:channelsPlot[-1]+1]/ Messung.liveTimeBackground,
            yerr=Messung.messreiheBackgroundError[channelsPlot[0]:channelsPlot[-1]+1]/ Messung.liveTimeBackground,
            fmt="."
            )
        #Durchschnitt des Hintergrunds
        if False:
            axs[0].hlines(
                np.mean(Messung.messreiheBackground[channelsPlot[0]:channelsPlot[-1]+1]/Messung.liveTimeBackground),
                min(channelsPlot),
                max(channelsPlot),
                color="indigo"
            )

        #Anpassung an die Gaußkurve
        axs[0].plot(
            np.linspace(channelsPlot[0],channelsPlot[-1],1000),
            gf(myOdr.output.beta,np.linspace(channelsPlot[0],channelsPlot[-1],1000))
            )
        #Nulllinie Residuenplots
        axs[1].hlines(0,channelsPlot[0],channelsPlot[-1])

        axs[0].title.set_text("Anpassung und Residuenplot: " + Messung.typ + " " + Messung.parameter + " " + extraTitle )
        axs[1].set_xlabel("Channel")
        axs[0].set_ylabel("Intensität pro Zeit [1/s]")
        axs[1].set_ylabel("Intensität pro Zeit - Fit [1/s]")
        
        if Set.saveOrShow:
            fig.savefig("../plots/anpassung" + Messung.typ + Messung.parameter + extraTitle)
        else: fig.show()
    if not halbwert:   
        return myOdr
    else: 
        return myOdr, [deltaChannel, deltaChannelError]

def linAnpassung( xValues, yValues, exValues=None, eyValues=None,plot=False,name="",title="Energie zu Channel"):
    guess = [(max(yValues)-min(yValues))/(max(xValues)-min(xValues)),0]

    myData = odr.RealData(xValues,yValues, sx=exValues, sy=eyValues)
    myOdr = odr.ODR(myData, linear, beta0=guess)
    myOdr.run()
    if Set.printAnpassungLin:
        print("lineare Anpassung")
        print("chiq/ndof:   ",myOdr.output.res_var)
        print("Datenpunkte: ", len(xValues))
        #print(myOdr.output.beta)
        #myOdr.output.pprint()
        print("")
    
    # chiq/ndof = resVar 
    if plot:
        if type(exValues) == type(None):
            exValues = np.array([0]*len(xValues))
        if type(eyValues) == type(None):
            eyValues = np.array([0]*len(yValues))
        
        fig, axs = plt.subplots(2,1, sharex=True, figsize=(20,10),gridspec_kw={'height_ratios': [3,1]})
        
        dataY = [
            yValues,
            np.array(yValues)-lf(myOdr.output.beta,np.array(xValues))
            ]
        for i in range(len(dataY)):
            axs[i].grid()

            axs[i].errorbar(
                xValues,
                dataY[i],
                yerr=eyValues,
                xerr=exValues,
                fmt=".")


        axs[0].plot(np.linspace(min(xValues),max(xValues)), lf(myOdr.output.beta,np.linspace(min(xValues),max(xValues))))
        axs[1].hlines(0,min(xValues),max(xValues))
        axs[0].title.set_text("Anpassung und Residuenplot: "+ title + " " + name )
        if Set.saveOrShow:
            fig.savefig("../plots/linAnpassungEnergie" + name  )
        else: 
            fig.show()
    return myOdr 

def quadAnpassung(xValues, yValues, exValues=None, eyValues=None):
    guess = [1,1]
    myData = odr.RealData(xValues,yValues, sx=exValues, sy=eyValues)
    myOdr = odr.ODR(myData, quadratic, beta0=guess)
    myOdr.run()

    xPlot = np.linspace(min(xValues),max(xValues))
    plt.figure(figsize=(20,10))
    plt.plot(xPlot,qf(myOdr.output.beta,xPlot))
    plt.errorbar(xValues,yValues,fmt=".",yerr=eyValues, xerr=exValues)
    plt.savefig("../plots/quadFit")
    return myOdr

def effizienz(Probe, m, err_m, nah=True, err_stat=True):  
    
    #A=Aktivität, I=Photonenausbeute, r=Abstand Strahler<->Szintillator, m=Zählrate des Detektors 
    A = Probe.a
    err_A = Probe.ae
    I = Probe.peaksIntensity
    err_I = [0 for i in range(len(I))] # keinen Fehler auf die Intensität

    if nah==True:
        r = 5.59*10**(-2)
        if not Set.eigeneDaten:
            r=4.8*10**(-2) #Alex Abstand
    else:
        r = 27.9*10**(-2)
    
    if err_stat==True:
        if nah==True:
            std_r = 0.0005/np.sqrt(12)
        else:
            std_r = 0.05/np.sqrt(12)
    else:
        if nah==True:
            std_r = 0
        else:
            std_r = 0.3+0.2*r/np.sqrt(3)
        
    #Fläche des Szintillators
    d = [8.085, 8.085, 8.075, 8.065]
    mean_d = np.mean(d)*10**(-2)
    
    if err_stat==True:
        std_d = np.std(d)*10**(-2)
    else:
        std_d = 0
    
    F_D = np.pi*(mean_d/2)**2
    std_F_D = np.pi/2*mean_d*std_d
    
    epsilon = []
    std_epsilon = []
    for i in range(len(m)):    
        epsilon += [4*np.pi*r**2/(A*I[i]/100)*m[i]/F_D]
        std_epsilon += [err_epsilon(float(r), float(std_r), float(A), float(err_A), float(I[i]/100), float(err_I[i]/100), float(m[i]), float(err_m[i]), float(F_D), float(std_F_D))]
    
    if Set.printOutputEffizenz:

        print("\nProbe:       {0}".format(Probe.name))
        if nah: print("Entfernung:  nah")
        else: print("Entfernung:  fern")
        print("Aktivität:   {0} ± {1}\n".format(A,err_A))
        for i in range(len(m)):
            print("Intensität:              {0:.3f} ± {1:.3f}".format(I[i],err_I[i]))
            print("Ereignisse pro Zeit:     {0:.3f} ± {1:.3f}".format(m[i], err_m[i]))
            print("Effektivität:            ({0:.1f} ± {1:.1f})%\n".format(epsilon[i]*10**2,std_epsilon[i]*10**2))
        
        print("\n")

    return epsilon, std_epsilon

#Theoretische Kurve zum Energie-Winkel-Zsmhang
def energyWinkelKurve(energyPhoton_vorher, Winkel):

    a = energyPhoton_vorher/(511)
    energyPhoton_nachher = energyPhoton_vorher/(1+a*(1-np.cos(np.deg2rad(Winkel))))
    
    return energyPhoton_nachher


#Wirkunsgquerschnitt
def wirkungsquerschnitt(Probe, peakEnergy, epsilon, err_epsilon, m, m_err, err_stat=True):

    A = Probe.a
    err_A = Probe.ae
    I = Probe.peaksIntensity[0]
    #err_I = [0 for i in range(len(I))] # keinen Fehler auf die Intensität
    err_I = 0

    #Abstand Streukörper Szinitllator
    r = (12.2+29.9)*10**(-2)
    
    if err_stat==True:
        std_r = 0.1/np.sqrt(12) #richtiger Wert????????????????????????????????????????????????????????
    else:
        std_r = 0.3+0.2*r/np.sqrt(3) #richtiger Wert????????????????????????????????????????????????????????
    
    #Abstand Strahler Streukörper
    r_0 = 4.7*10**(-2)
    
    if err_stat==True:
        std_r0 = 0.1/np.sqrt(12)*10**(-2) #richtiger Wert????????????????????????????????????????????????????????
    else:
        std_r0 = 0.3+0.2*r_0/np.sqrt(3) #richtiger Wert????????????????????????????????????????????????????????
       
    #Fläche des Szintillators
    d = [8.085, 8.085, 8.075, 8.065]
    mean_d = np.mean(d)*10**(-2)
    
    if err_stat==True:
        std_d = np.sqrt((np.std(d)*10**(-2))**2+(0.005/np.sqrt(12)*10**(-2))**2) #??????????????????????????????
    else:
        std_d = 0 #??????????????????????????????????????????????
    
    F_D = float(np.pi*(mean_d/2)**2)
   
    std_F_D = np.pi/2*mean_d*std_d

    #Weg im Streukörper
    d_Als = [1.200, 1.200, 1.205]
    r_Al = np.mean(d_Als)*10**(-2)/2 

    if err_stat:
        err_rAl = np.std(d_Als)*10**(-2) #???????????????????????????????????????????????????
    else:
        err_rAl = 0 #????????????????????????????????????????????????

    #Höhe Streukörper
    hs = [2.105, 2.110, 2.110]
    h = np.mean(hs)*10**(-2)
    if err_stat:
        err_h = np.sqrt(np.std(hs)**2+0.005*(np.sqrt(12)))*10**(-2) #???????????????????????????????????????????????????????????
    else:
        err_h = err_rAl


    #Dichten
    rho_Al = 2.699
    rho_N = 1.332*10**(-3)
    rho_O = 0

    #mus vor Streuung
    mu_N  = 8.063*10**(-2)*rho_N 
    mu_O  = 8.070*10**(-2)*rho_O
    mu_Luft = 0.78*mu_N+0.22*mu_O
    mu_Al = 7.802*10**(-2)*rho_Al

    #mus nach Streuung
    energy = int(peakEnergy/100) + 0.5

    if energy == 2:
        mu_Al_nach  = 1.223*10**(-1)*rho_Al
        mu_N_nach = 1.233*10**(-1)*rho_N
        mu_O_nach = 1.237*10**(-1)*rho_O
    elif energy == 3:
        mu_Al_nach  = 1.042*10**(-1)*rho_Al
        mu_N_nach = 1.068*10**(-1)*rho_N
        mu_O_nach = 1.070*10**(-1)*rho_O
    elif energy == 4:
        mu_Al_nach  = 9.276*10**(-2)*rho_Al
        mu_N_nach = 9.557*10**(-2)*rho_N
        mu_O_nach = 9.566*10**(-2)*rho_O
    elif energy == 5:
        mu_Al_nach  = 8.445*10**(-2)*rho_Al
        mu_N_nach = 8.719*10**(-2)*rho_N
        mu_O_nach = 8.729*10**(-2)*rho_O
    else:
        mu_Al_nach = mu_Al
        mu_N_nach = mu_N
        mu_O_nach = mu_O
    
    mu_Luft_nach = 0.78*mu_N_nach+0.22*mu_O_nach

    #Absorption
    n = np.exp(-mu_Luft*r_0)*np.exp(-mu_Al*r_Al)*np.exp(-mu_Al_nach*r_Al)*np.exp(-mu_Luft_nach*r)
    n_err = absorption_err(mu_Luft, mu_Luft_nach, mu_Al, mu_Al_nach, float(r_0), float(std_r0), float(r_Al), float(err_rAl), float(r), float(std_r), float(r_Al), float(err_rAl))
    
    #N_e
    Z = 13
    u = physical_constants["atomic unit of mass"][0]
    m_Al = 26.9815384*u
    N_e = Z*np.pi*r_Al**2*h*rho_Al/m_Al
    err_N_e = N_e_err(r_Al, err_rAl, h, err_h, m_Al, rho_Al, Z)
    
    #Wirkungsquerschnitt
    wirkung = r**2/F_D*m/(n*epsilon*N_e)*4*np.pi*r_0**2/(A*I)
    wirkung_err = err_wirkung(float(r), float(std_r), float(r_Al), float(err_rAl), float(F_D), float(std_F_D), float(m), float(m_err), float(n), float(n_err), float(epsilon), float(err_epsilon), float(N_e), float(err_N_e), float(r_0), float(std_r0), float(r_Al), float(err_rAl), float(A), float(err_A), float(I), float(err_I))

    return wirkung, wirkung_err.evalf()





def kleinNishaFormel(ePhoton_vorher, ePhoton_nachher, winkel):
    alpha = e**2/(4*pi)
    return 1/2 * alpha**2 / m_e**2 *(ePhoton_nachher/ePhoton_vorher)**2*(ePhoton_nachher/ePhoton_vorher + ePhoton_vorher/ePhoton_nachher -np.sin(winkel)**2)